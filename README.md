# Template Sidenav Bootstrap

Un template de départ fait avec bootstrap 4.1 pour démarrer un site web.

### Caractéristiques

* Toolbar responsive fixed-top
* Bouton à gauche dans la toolbar permettant de cacher ou montrer la sidenav
* Dès qu'on passe en dessous des 768px, la sidenav est cachée et la toolbar passe en mode responsive